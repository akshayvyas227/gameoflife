package swiggy.bootcamp;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

// Represents attribute of the single element of grid in Game of Life
public class Cell {

    private final int xCoordinate;
    private final int yCoordinate;
    private final int state;
    private static final int LIFE_GIVING_NUMBER = 3;
    private static final int LONELINESS_THRESHOLD = 2;
    private static final int SUFFOCATION_THRESHOLD = 3;
    private static final int UNIT_NEIGHBOUR_DISTANCE = 1;

    public Cell(int xCoordinate, int yCoordinate, int state) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.state = state;
    }

    boolean isDead() {
        return state == 0;
    }

    boolean isAlive() {
        return state == 1;
    }

    private boolean isNoState() {
        return state == 2;
    }

    private Cell aliveCell(int xCoordinate, int yCoordinate) {
        return new Cell(xCoordinate, yCoordinate, 1);
    }

    private Cell deadCell(int xCoordinate, int yCoordinate) {
        return new Cell(xCoordinate, yCoordinate, 0);
    }

    private Cell NoStateCell(int xCoordinate, int yCoordinate) {
        return new Cell(xCoordinate, yCoordinate, 2);
    }

    Cell tick(Cells cells) {
        int countOfAliveNeighbours = (int) this.getNeighboursWithoutState()
                .stream()
                .filter(cell -> cells.getCellWithState(cell.xCoordinate, cell.yCoordinate).isAlive())
                .count();

        if (this.isNoState()) {
            return tickForNoStateCell(cells, countOfAliveNeighbours);
        }
        return tickForDefinedStateCell(countOfAliveNeighbours);
    }

    private Cell tickForNoStateCell(Cells cells, int countOfAliveNeighbours) {
        Cell foundedStateCell = cells.getCellWithState(this.xCoordinate, this.yCoordinate);
        if (isAliveInNextGeneration(foundedStateCell, countOfAliveNeighbours))
            return aliveCell(xCoordinate, yCoordinate);
        return deadCell(xCoordinate, yCoordinate);
    }

    private Cell tickForDefinedStateCell(int countOfAliveNeighbours) {
        if (isAliveInNextGeneration(this, countOfAliveNeighbours))
            return aliveCell(xCoordinate, yCoordinate);
        return deadCell(xCoordinate, yCoordinate);
    }

    Set<Cell> getNeighboursWithoutState() {
        Set<Cell> cellNeighbours = new HashSet<>();

        cellNeighbours.add(NoStateCell(this.xCoordinate - UNIT_NEIGHBOUR_DISTANCE, this.yCoordinate - UNIT_NEIGHBOUR_DISTANCE));
        cellNeighbours.add(NoStateCell(this.xCoordinate - UNIT_NEIGHBOUR_DISTANCE, this.yCoordinate));
        cellNeighbours.add(NoStateCell(this.xCoordinate - UNIT_NEIGHBOUR_DISTANCE, this.yCoordinate + UNIT_NEIGHBOUR_DISTANCE));
        cellNeighbours.add(NoStateCell(this.xCoordinate, this.yCoordinate - UNIT_NEIGHBOUR_DISTANCE));
        cellNeighbours.add(NoStateCell(this.xCoordinate, this.yCoordinate + UNIT_NEIGHBOUR_DISTANCE));
        cellNeighbours.add(NoStateCell(this.xCoordinate + UNIT_NEIGHBOUR_DISTANCE, this.yCoordinate - UNIT_NEIGHBOUR_DISTANCE));
        cellNeighbours.add(NoStateCell(this.xCoordinate + UNIT_NEIGHBOUR_DISTANCE, this.yCoordinate));
        cellNeighbours.add(NoStateCell(this.xCoordinate + UNIT_NEIGHBOUR_DISTANCE, this.yCoordinate + UNIT_NEIGHBOUR_DISTANCE));

        return cellNeighbours;
    }

    private boolean isAliveInNextGeneration(Cell cell, int countOfAliveNeighbours) {
        if (cell.isAlive())
            return (countOfAliveNeighbours >= LONELINESS_THRESHOLD && countOfAliveNeighbours <= SUFFOCATION_THRESHOLD);
        return countOfAliveNeighbours == LIFE_GIVING_NUMBER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return xCoordinate == cell.xCoordinate &&
                yCoordinate == cell.yCoordinate &&
                state == cell.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xCoordinate, yCoordinate, state);
    }
}
