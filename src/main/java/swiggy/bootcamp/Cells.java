package swiggy.bootcamp;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

// Represents Collection of single elements of grid in GameOfLife
class Cells {
    private final Set<Cell> cells;

    Cells(Set<Cell> cells) {
        this.cells = cells;
    }

    Cells tick() {
        Set<Cell> newCells = new HashSet<>();
        for (Cell cell : cells) {
            Cell cellNextGen = cell.tick(this);
            if (cellNextGen.isAlive()) newCells.add(cellNextGen);
            newCells.addAll(tickNeighbours(cell));
        }
        return new Cells(newCells);
    }

    private Set<Cell> tickNeighbours(Cell centralCell) {
        Set<Cell> cellNeighbours = centralCell.getNeighboursWithoutState();
        Set<Cell> nextGenAliveCells = new HashSet<>();
        for (Cell cell : cellNeighbours) {
            Cell cellNextGen = cell.tick(this);
            if (cellNextGen.isAlive()) nextGenAliveCells.add(cellNextGen);
        }
        return nextGenAliveCells;
    }

    Cell getCellWithState(int xCoordinate, int yCoordinate) {
        Cell temporaryCell = new Cell(xCoordinate, yCoordinate, 1);
        if (cells.contains(temporaryCell)) return temporaryCell;

        return new Cell(xCoordinate, yCoordinate, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cells cells1 = (Cells) o;
        return cells.equals(cells1.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cells);
    }
}
