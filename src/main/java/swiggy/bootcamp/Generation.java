package swiggy.bootcamp;

import java.util.Objects;

// Represents whole bunch of cells alive in the present
class Generation {
    private final Cells cells;

    Generation(Cells cells) {
        this.cells = cells;
    }

    Generation tick() {
        return new Generation(cells.tick());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Generation that = (Generation) o;
        return cells.equals(that.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cells);
    }
}
