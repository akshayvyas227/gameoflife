package swiggy.bootcamp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CellTest {
    @Test
    @DisplayName("A lonely Cell will die by loneliness")
    void singleCellDieByLoneliness() {
        Cell cell = new Cell(1, 1, 1);
        Set<Cell> cells = new HashSet<>();

        cells.add(cell);

        assertTrue(cell.tick(new Cells(cells)).isDead());
    }

    @Test
    @DisplayName("A single Cell Stays alive when have 2 alive neighbours")
    void singleCellStaysAliveWithTwoAliveNeighbours() {
        Cell cell = new Cell(1, 1, 1);
        Cell neighbourCell = new Cell(0, 1, 1);
        Cell otherNeighbourCell = new Cell(1, 0, 1);
        Set<Cell> cells = new HashSet<>();

        cells.add(cell);
        cells.add(neighbourCell);
        cells.add(otherNeighbourCell);

        assertTrue(cell.tick(new Cells(cells)).isAlive());
    }

    @Test
    @DisplayName("A single Cell Stays alive when have 3 alive neighbours")
    void singleCellStaysAliveWithThreeAliveNeighbours() {
        Cell cell = new Cell(1, 1, 1);
        Cell neighbourCellOne = new Cell(0, 1, 1);
        Cell neighbourCellTwo = new Cell(1, 0, 1);
        Cell neighbourCellThree = new Cell(1, 2, 1);
        Set<Cell> cells = new HashSet<>();

        cells.add(cell);
        cells.add(neighbourCellOne);
        cells.add(neighbourCellTwo);
        cells.add(neighbourCellThree);

        assertTrue(cell.tick(new Cells(cells)).isAlive());
    }

    @Test
    @DisplayName("A single dead Cell remains dead when no alive neighbour cell is present")
    void singleDeadCellRemainsDeadWhenNoAliveNeighbour() {
        Cell cell = new Cell(2, 2, 0);
        Set<Cell> cells = new HashSet<>();
        cells.add(cell);

        assertTrue(cell.tick(new Cells(cells)).isDead());
    }

    @Test
    @DisplayName("a Dead Cell comes to live when have 3 live neighbours")
    void singleDeadCellComesToLive() {
        Cell cell = new Cell(2, 2, 0);
        Cell cellNeighbour1 = new Cell(2, 3, 1);
        Cell cellNeighbour2 = new Cell(3, 2, 1);
        Cell cellNeighbour3 = new Cell(1, 1, 1);
        Set<Cell> cells = new HashSet<>();

        cells.add(cell);
        cells.add(cellNeighbour1);
        cells.add(cellNeighbour2);
        cells.add(cellNeighbour3);

        assertTrue(cell.tick(new Cells(cells)).isAlive());
    }

    @Test
    @DisplayName("a Dead Cell remains Dead when have 2 live neighbours")
    void singleDeadRemainsDeadWhenHaveTwoAliveNeighbour() {
        Cell cell = new Cell(2, 2, 0);
        Cell cellNeighbour1 = new Cell(2, 3, 1);
        Cell cellNeighbour2 = new Cell(3, 2, 1);
        Set<Cell> cells = new HashSet<>();

        cells.add(cell);
        cells.add(cellNeighbour1);
        cells.add(cellNeighbour2);

        assertTrue(cell.tick(new Cells(cells)).isDead());
    }

    @Test
    @DisplayName("A alive Cell becomes dead when have 4 alive neighbours Due to Suffocation")
    void aliveCellBecomesDeadWhenHaveFourAliveNeighbour() {
        Cell cell = new Cell(1, 1, 1);
        Cell cellNeighbour1 = new Cell(0, 0, 1);
        Cell cellNeighbour2 = new Cell(0, 1, 1);
        Cell cellNeighbour3 = new Cell(1, 0, 1);
        Cell cellNeighbour4 = new Cell(2, 2, 1);
        Set<Cell> cells = new HashSet<>();

        cells.add(cell);
        cells.add(cellNeighbour1);
        cells.add(cellNeighbour2);
        cells.add(cellNeighbour3);
        cells.add(cellNeighbour4);

        assertTrue(cell.tick(new Cells(cells)).isDead());
    }
}