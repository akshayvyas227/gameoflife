package swiggy.bootcamp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CellsTest {

    @Test
    @DisplayName("Single Cell in Cells die of loneliness")
    void SinglCellinCellsDie() {
        Cell cell = new Cell(1, 1, 1);
        Set<Cell> cells = new HashSet<>();
        Set<Cell> nextCells = new HashSet<>();

        cells.add(cell);

        assertEquals(new Cells(nextCells), new Cells(cells).tick());
    }

    @Test
    @DisplayName("Cells in Block Pattern Still Life remain alive in next Generation also")
    void BlockPatternStillLife() {
        Set<Cell> cells = new HashSet<>();
        Cell cell1 = new Cell(1, 1, 1);
        Cell cell2 = new Cell(1, 2, 1);
        Cell cell3 = new Cell(2, 1, 1);
        Cell cell4 = new Cell(2, 2, 1);
        Set<Cell> nextGenCells = new HashSet<>();
        Cell nextGenCell1 = new Cell(1, 1, 1);
        Cell nextGencell2 = new Cell(1, 2, 1);
        Cell nextGencell3 = new Cell(2, 1, 1);
        Cell nextGencell4 = new Cell(2, 2, 1);

        cells.add(cell1);
        cells.add(cell2);
        cells.add(cell3);
        cells.add(cell4);
        nextGenCells.add(nextGenCell1);
        nextGenCells.add(nextGencell2);
        nextGenCells.add(nextGencell3);
        nextGenCells.add(nextGencell4);

        assertEquals(new Cells(nextGenCells), new Cells(cells).tick());
    }

    @Test
    @DisplayName("Cells in Boat Pattern Still Life remain alive in next Generation also")
    void BoatPatternStillLife() {
        Set<Cell> cells = new HashSet<>();
        Cell cell1 = new Cell(0, 1, 1);
        Cell cell2 = new Cell(1, 0, 1);
        Cell cell3 = new Cell(2, 1, 1);
        Cell cell4 = new Cell(0, 2, 1);
        Cell cell5 = new Cell(1, 2, 1);
        Set<Cell> nextGenCells = new HashSet<>();
        Cell nextGenCell1 = new Cell(0, 1, 1);
        Cell nextGencell2 = new Cell(1, 0, 1);
        Cell nextGencell3 = new Cell(2, 1, 1);
        Cell nextGencell4 = new Cell(0, 2, 1);
        Cell nextGencell5 = new Cell(1, 2, 1);

        cells.add(cell1);
        cells.add(cell2);
        cells.add(cell3);
        cells.add(cell4);
        cells.add(cell5);
        nextGenCells.add(nextGenCell1);
        nextGenCells.add(nextGencell2);
        nextGenCells.add(nextGencell3);
        nextGenCells.add(nextGencell4);
        nextGenCells.add(nextGencell5);

        assertEquals(new Cells(nextGenCells), new Cells(cells).tick());
    }

    @Test
    @DisplayName("Cells in Toad Pattern Two phase Oscillator")
    void ToadPatternTwoPhaseOscillator() {
        Set<Cell> cells = new HashSet<>();
        Cell cell1 = new Cell(1, 1, 1);
        Cell cell2 = new Cell(1, 2, 1);
        Cell cell3 = new Cell(1, 3, 1);
        Cell cell4 = new Cell(2, 2, 1);
        Cell cell5 = new Cell(2, 3, 1);
        Cell cell6 = new Cell(2, 4, 1);
        Set<Cell> nextGenCells = new HashSet<>();
        Cell nextGenCell1 = new Cell(0, 2, 1);
        Cell nextGencell2 = new Cell(1, 1, 1);
        Cell nextGencell3 = new Cell(1, 4, 1);
        Cell nextGencell4 = new Cell(2, 1, 1);
        Cell nextGencell5 = new Cell(2, 4, 1);
        Cell nextGencell6 = new Cell(3, 3, 1);

        cells.add(cell1);
        cells.add(cell2);
        cells.add(cell3);
        cells.add(cell4);
        cells.add(cell5);
        cells.add(cell6);
        nextGenCells.add(nextGenCell1);
        nextGenCells.add(nextGencell2);
        nextGenCells.add(nextGencell3);
        nextGenCells.add(nextGencell4);
        nextGenCells.add(nextGencell5);
        nextGenCells.add(nextGencell6);

        assertEquals(new Cells(nextGenCells), new Cells(cells).tick());
    }


    @Test
    @DisplayName("Cells in Blinker Pattern Oscillator")
    void BlinkerPatternOscillator() {
        Set<Cell> cells = new HashSet<>();
        Cell cell1 = new Cell(1, 1, 1);
        Cell cell2 = new Cell(1, 0, 1);
        Cell cell3 = new Cell(1, 2, 1);
        Set<Cell> nextGenCells = new HashSet<>();
        Cell nextGenCell1 = new Cell(1, 1, 1);
        Cell nextGencell2 = new Cell(0, 1, 1);
        Cell nextGencell3 = new Cell(2, 1, 1);

        cells.add(cell1);
        cells.add(cell2);
        cells.add(cell3);
        nextGenCells.add(nextGenCell1);
        nextGenCells.add(nextGencell2);
        nextGenCells.add(nextGencell3);

        assertEquals(new Cells(nextGenCells), new Cells(cells).tick());
    }

}
