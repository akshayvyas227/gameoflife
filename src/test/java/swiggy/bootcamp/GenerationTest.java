package swiggy.bootcamp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GenerationTest {

    @Test
    @DisplayName("Single Cell in Cells die of loneliness")
    void SinglCellinCellsDie() {
        Cell cell = new Cell(1, 1, 1);
        Set<Cell> cells = new HashSet<>();
        Set<Cell> nextCells = new HashSet<>();

        cells.add(cell);

        assertEquals(new Generation(new Cells(nextCells)), new Generation(new Cells(cells)).tick());
    }


}
